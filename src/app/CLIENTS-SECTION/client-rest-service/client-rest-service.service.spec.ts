import { TestBed, inject } from '@angular/core/testing';

import { ClientRestService } from './client-rest-service.service';

describe('ClientRestServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientRestService]
    });
  });

  it('should be created', inject([ClientRestService], (service: ClientRestService) => {
    expect(service).toBeTruthy();
  }));
});
