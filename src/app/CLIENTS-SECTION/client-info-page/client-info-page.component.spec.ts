import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInfoPageComponent } from './client-info-page.component';

describe('ClientInfoPageComponent', () => {
  let component: ClientInfoPageComponent;
  let fixture: ComponentFixture<ClientInfoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientInfoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInfoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
