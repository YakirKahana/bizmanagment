import { Component, OnInit } from '@angular/core';
import { SupplierRestService } from '../supplier-rest-service/supplier-rest-service.service';
import { SupplierProductsInterface } from '../supplier-rest-service/supplier-products-interface';
import { ActivatedRoute,Params} from '@angular/router';

@Component({
  selector: 'app-supplier-orders',
  templateUrl: './supplier-orders.component.html',
  styleUrls: ['./supplier-orders.component.css'],

})
export class SupplierOrdersComponent implements OnInit {
  companyName;
  orders;
  constructor(private rest:SupplierRestService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.companyName = this.route.parent.snapshot.params['companyName'];

    let ordersTemp = this.rest.orders.filter(obj => obj.companyName ==   this.companyName);
    //get the actuall product data without the companyName and assign it to the "SupplierProducts" var

    this.orders = ordersTemp[0].thisOrders;
    console.log(this.orders);
  }

  addClasses(orderRecived){
    if(orderRecived === true){
      return "glyphicon-ok";
    }else{
      return "glyphicon-remove";
    }
  }

}
