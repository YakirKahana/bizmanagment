import { Component, OnInit,Pipe,PipeTransform } from '@angular/core';
import { ActivatedRoute,Params} from '@angular/router';
import { SupplierRestService } from '../supplier-rest-service/supplier-rest-service.service';
import { SupplierProductsInterface } from '../supplier-rest-service/supplier-products-interface';

@Component({
  selector: 'app-new-edit-product',
  templateUrl: './new-edit-product.component.html',
  styleUrls: ['./new-edit-product.component.css'],
  providers:[SupplierRestService]
})
export class SupplierNewEditProductComponent implements OnInit {
  numberOfNewProducts:number = 1;//nubmer of iteration for ngFor. ngFor implemented for adding number of products at once.
  supplier; //route parameter of the supplier
  productName; //route parameter of the product name if = "new" will become a new product page

  pricesWithVAT :number[]=[];//array of the prices With VAT
  pricesWithoutVAT :number[]=[]; //array of the prices without VAT

  productToEdit;
  constructor(private route : ActivatedRoute, private rest:SupplierRestService) { }

  ngOnInit() {
    //get route parameters and set them to to productName and to suppliers variables
    this.route.params.subscribe((params:Params)=>{
        this.productName = params['productName'];
        this.supplier = params['id'];
        if(this.productName != 'new'){
          let SupplierProductsObj : SupplierProductsInterface[] =[];

          SupplierProductsObj = this.rest.productsList.filter(obj => obj.companyName ==   this.supplier);
          let SupplierProducts = SupplierProductsObj[0].products;
          this.productToEdit =SupplierProducts.filter(obj2=> obj2.productName == this.productName);
          this.pricesWithoutVAT[0] = this.productToEdit[0].price;
          this.addVAT(0);
          // console.log(this.productToEdit[0].price);

        }

    });
  }

  //this function will add iteration on ngFor
  anotherProduct(){
    this.numberOfNewProducts++
  }

//this function adds vat from vat arg i is for index of ngFor
  addVAT(i){
    //get price without vat
    let pwov :number =this.pricesWithoutVAT[i];
    // calc the amount of vat
    let vat:number = (17/100)+1;
    console.log(vat);
    //add vat to price
    let pwv:number = pwov * vat;
    //set price with vat
    this.pricesWithVAT[i] =pwv;

  }

  //this function removes vat from vat arg i is for index of ngFor
  removeVAT(i){
    //get price with vat
    let pwv:number = this.pricesWithVAT[i];
    // calc the amount of vat
    let vat :number= (17/100)+1;
    //subtract vat from price
    let pwov:number = pwv / vat;
    //set the price without vat
    this.pricesWithoutVAT[i] = pwov;

  }
}

//set pipe loop, make ngFor work with a bumner of iterations
@Pipe({name: 'loop'})
export class LoopNumberPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let res = [];
    for (let i = 0; i < value; i++) {
        res.push(i);
      }
      return res;
  }
}
