import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Params} from '@angular/router';
import {ClientRestService } from '../client-rest-service/client-rest-service.service';

@Component({
  selector: 'app-clients-orders',
  templateUrl: './clients-orders.component.html',
  styleUrls: ['./clients-orders.component.css']
})
export class ClientsOrdersComponent implements OnInit {
  routeID;
  orders;
  constructor(private route:ActivatedRoute, private rest:ClientRestService) { }

  ngOnInit() {
    this.routeID = this.route.parent.snapshot.params['clientName'];
    let temp = this.rest.orders.filter(obj => obj.clientName ==   this.routeID);
    this.orders = temp[0].clientOrders;
    console.log(this.orders);
  }

  addClasses(orderRecived){
    if(orderRecived === true){
      return "glyphicon-ok";
    }else{
      return "glyphicon-remove";
    }
  }

}
