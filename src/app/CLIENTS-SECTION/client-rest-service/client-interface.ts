export interface ClientInterface {
  clientName:string,
  contactName:string,
  contactPhone:number,
  phone:number,
  phone2?:number,
  fax?:number,
  email?:string,
  website?:string,
  address?:{
    city:string,
    street:string,
    streetNumber?:number,
    apartmentNumber?:number,
    zip?:number
  },
  openingHouer:string,
  closingHouer:string,
  optional?:any,
  optional2?:any,
  optional3?:any,

}
