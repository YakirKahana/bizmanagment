import { Component, OnInit } from '@angular/core';
// import supplier rest sercive that gets supplier data from the server
import { SupplierRestService } from '../supplier-rest-service/supplier-rest-service.service';
import { SupplierProductsInterface } from '../supplier-rest-service/supplier-products-interface';
import { ActivatedRoute,Params} from '@angular/router';
import { FilterPipe } from '../../filter.pipe';

@Component({
  selector: 'app-supplier-product-list',
  templateUrl: './supplier-product-list.component.html',
  styleUrls: ['./supplier-product-list.component.css'],


})
export class SupplierProductListComponent implements OnInit {
  term; // term for ngFor- the term to filter by.
  companyName; // router parameter to set in HTML links

  SupplierProducts =[];// store the data of the products without the company name.
  constructor(private rest :SupplierRestService,private route :ActivatedRoute) { }

  ngOnInit() {
    //get route parameter and assign it in the companyName var.
     this.route.parent.params.subscribe((params:Params)=>{
       this.companyName = params['companyName'];
     });
     let SupplierProductsObj : SupplierProductsInterface[] =[];//store the data of the supplierProducts "as is" from the rest service

    //  get the data from rest service by the companyName var  and assign it in the SupplierProductsObj let.
     SupplierProductsObj = this.rest.productsList.filter(obj => obj.companyName ==   this.companyName);
     //get the actuall product data without the companyName and assign it to the "SupplierProducts" var
     this.SupplierProducts = SupplierProductsObj[0].products;

  }

  //this function calculate the VAT and subtract it from the price to display the price without the VAT
  calcVAT(number,VATPrecent){
    return number-(number/100*VATPrecent);
  }

}
