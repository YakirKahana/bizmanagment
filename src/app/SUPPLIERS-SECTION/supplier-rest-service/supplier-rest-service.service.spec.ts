import { TestBed, inject } from '@angular/core/testing';

import { SupplierRestService } from './supplier-rest-service.service';

describe('SupplierRestServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupplierRestService]
    });
  });

  it('should be created', inject([SupplierRestService], (service: SupplierRestService) => {
    expect(service).toBeTruthy();
  }));
});
