/* this service will get/send supplliers related date from/to the server- suppliers info. suppliers products*/

import { Injectable } from '@angular/core';
import { SuppliersInterface } from './suppliers-interface';
import { SupplierProductsInterface } from './supplier-products-interface';
@Injectable()
export class SupplierRestService {

  constructor() { }

  suppliersList: SuppliersInterface[] = [
    //hard coded dummy data for testing. will be replaced once serverside implemented.
    {
      companyName: 'name1',
      contactName: 'contactName1',
      contactPhone: 9725111111,
      phone: 9723111111,
      phone2: 9724111111,
      fax: 9726111111,
      email: 'company1@mail.com',
      website: 'company1.com',
      address: {
        city: 'city1',
        street: 'street1',
        streetNumber: 1,
        apartmentNumber: 1,
        zip: 111111
      },
      openingHouer: '11:11',
      closingHouer: '23:11',
      optional: 1,
      optional2: 1,
      optional3: 1,
    },
    {
      companyName: 'חברה 2',
      contactName: 'contactName2',
      contactPhone: 9725222222,
      phone: 9723222222,
      phone2: 9724222222,
      fax: 9726222222,
      email: 'company2@mail.com',
      website: 'company2.com',
      address: {
        city: 'city2',
        street: 'street2',
        streetNumber: 2,
        apartmentNumber: 2,
        zip: 222222
      },
      openingHouer: '2:22',
      closingHouer: '22:22',
      optional: 2,
      optional2: 2,
      optional3: 2,
    },
    {
      companyName: 'name3',
      contactName: 'contactName3',
      contactPhone: 9725333333,
      phone: 9723333333,
      phone2: 9724333333,
      fax: 9726333333,
      email: 'company3@mail.com',
      website: 'company3.com',
      address: {
        city: 'city3',
        street: 'street3',
        streetNumber: 3,
        apartmentNumber: 3,
        zip: 333333
      },
      openingHouer: '3:33',
      closingHouer: '13:33',
    },
    {
      companyName: 'name4',
      contactName: 'contactName4',
      contactPhone: 9725444444,
      phone: 9723444444,
      phone2: 9724444444,
      fax: 9726444444,
      email: 'company4@mail.com',
      website: 'company4.com',
      address: {
        city: 'city4',
        street: 'street4',
        streetNumber: 4,
        apartmentNumber: 4,
        zip: 444444
      },
      openingHouer: '4:44',
      closingHouer: '4:44',
      optional: 1,
      optional2: 1,
      optional3: 1,
    },
    {
      companyName: 'name5',
      contactName: 'contactName5',
      contactPhone: 9725555555,
      phone: 9723555555,
      phone2: 9724555555,
      fax: 9726555555,
      email: 'company5@mail.com',
      website: 'company5.com',
      address: {
        city: 'city5',
        street: 'street5',
        streetNumber: 5,
        apartmentNumber: 5,
        zip: 555555
      },
      openingHouer: '5:55',
      closingHouer: '5:55',
      optional: 'sdfsd',
      optional2: 'dfsdfds',
      optional3: 'sdfsdfs',
    }


  ]


  /* suppliers products*/


  productsList: SupplierProductsInterface[] = [
    {
      companyName: 'name1',
      products: [{
        productName: 'name1-prod1',
        catalogNumber: 0,
        price: 5
      }, {
        productName: 'name1-prod2',
        catalogNumber: 1,
        price: 4
      }, {
        productName: 'name1-prod3',
        catalogNumber: 4,
        price: 1
      }]
    }, {
      companyName: 'חברה 2',
      products: [{
        productName: 'name2-prod1',
        catalogNumber: 0,
        price: 5
      }, {
        productName: 'name2-prod2',
        catalogNumber: 1,
        price: 4
      }, {
        productName: 'name2-prod3',
        catalogNumber: 4,
        price: 1
      }]
    }, {
      companyName: 'name3',
      products: [{
        productName: 'name3-prod1',
        catalogNumber: 0,
        price: 5
      }, {
        productName: 'name3-prod2',
        catalogNumber: 1,
        price: 4
      }, {
        productName: 'name3-prod3',
        catalogNumber: 4,
        price: 1
      }]
    }, {
      companyName: 'name4',
      products: [{
        productName: 'name4-prod1',
        catalogNumber: 0,
        price: 5
      }, {
        productName: 'name4-prod2',
        catalogNumber: 1,
        price: 4
      }, {
        productName: 'name4-prod3',
        catalogNumber: 4,
        price: 1
      }]
    }, {
      companyName: 'name5',
      products: [{
        productName: 'שם 5',
        catalogNumber: 0,
        price: 5
      }, {
        productName: 'שם 5 מוצר 2',
        catalogNumber: 1,
        price: 4
      }, {
        productName: 'נורה',
        catalogNumber: 2,
        price: 1
      }, {
        productName: 'מפסקים',
        catalogNumber: 3,
        price: 5
      }, {
        productName: 'בית נורה',
        catalogNumber: 4,
        price: 2.5
      }, {
        productName: 'תעלות',
        catalogNumber: 5,
        price: 51
      },]
    },
  ]

  orders = [
    {
      companyName: "name1",
      thisOrders: [{
        orderNumber: 0,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 500
      }
        , {
        orderNumber: 1,
        date: "3/03/2017",
        recevied: true,
        items: [],
        totalPrice: 5000
      }
        , {
        orderNumber: 2,
        date: "5/01/2013",
        recevied: false,
        items: [],
        totalPrice: 5320
      }
        , {
        orderNumber: 3,
        date: "01/02/2007",
        recevied: true,
        items: [],
        totalPrice: 2365
      }
        , {
        orderNumber: 4,
        date: "09/02/2013",
        recevied: true,
        items: [],
        totalPrice: 756
      }
        , {
        orderNumber: 5,
        date: "31/12/2015",
        recevied: false,
        items: [],
        totalPrice: 2000
      }
        , {
        orderNumber: 6,
        date: "12/04/2017",
        recevied: true,
        items: [],
        totalPrice: 2500
      }
        , {
        orderNumber: 7,
        date: "3/05/2017",
        recevied: true,
        items: [],
        totalPrice: 5600
      }]
    },{
      companyName: "חברה 2",
      thisOrders: [{
        orderNumber: 8,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 500
      }
        , {
        orderNumber: 9,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 2500
      }
        , {
        orderNumber: 10,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 50050
      }
        , {
        orderNumber: 11,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 5100
      }
        , {
        orderNumber: 12,
        date: "31/05/2007",
        recevied: true,
        items: [],
        totalPrice: 500
      }]
    }, {
      companyName: "name3",
      thisOrders: [{
        orderNumber: 8,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 500
      }
        , {
        orderNumber: 9,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 2500
      }
        , {
        orderNumber: 10,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 50050
      }
        , {
        orderNumber: 11,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 5100
      }
        , {
        orderNumber: 12,
        date: "31/05/2007",
        recevied: true,
        items: [],
        totalPrice: 500
      }]
    }, {
      companyName: "name4",
      thisOrders: [{
        orderNumber: 8,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 500
      }
        , {
        orderNumber: 9,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 2500
      }
        , {
        orderNumber: 10,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 50050
      }
        , {
        orderNumber: 11,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 5100
      }
        , {
        orderNumber: 12,
        date: "31/05/2007",
        recevied: true,
        items: [],
        totalPrice: 500
      }]
    }, {
      companyName: "name5",
      thisOrders: [{
        orderNumber: 8,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 500
      }
        , {
        orderNumber: 9,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 2500
      }
        , {
        orderNumber: 10,
        date: "31/05/2017",
        recevied: false,
        items: [],
        totalPrice: 50050
      }
        , {
        orderNumber: 11,
        date: "31/05/2017",
        recevied: true,
        items: [],
        totalPrice: 5100
      }
        , {
        orderNumber: 12,
        date: "31/05/2007",
        recevied: true,
        items: [],
        totalPrice: 500
      }]
    }]
    }
