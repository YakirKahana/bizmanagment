import { Component, OnInit } from '@angular/core';
import {ProductsRestService} from '../products-rest-service/products-rest-service.service';
import { ProductsInterface } from '../products-rest-service/products-interface';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-new-edit-product',
  templateUrl: './new-edit-product.component.html',
  styleUrls: ['./new-edit-product.component.css'],
  providers:[ProductsRestService]
})
export class NewEditProductComponent implements OnInit {
  numberOfNewProducts:number = 1;//nubmer of iteration for ngFor. ngFor implemented for adding number of products at once.

  productName; //route parameter of the product name if = "new" will become a new product page

  pricesWithVAT :number[]=[];//array of the prices With VAT
  pricesWithoutVAT :number[]=[]; //array of the prices without VAT

  productToEdit;
  constructor(private route : ActivatedRoute, private rest:ProductsRestService) { }

  ngOnInit() {
    //get route parameters and set them to to productName and to suppliers variables
    this.route.params.subscribe((params:Params)=>{
        this.productName = params['id'];

        if(this.productName != 'new'){


          this.productToEdit = this.rest.products.filter(obj=> obj.productName == this.productName);
          this.pricesWithoutVAT[0] = this.productToEdit[0].priceWithoutVAT;

          this.addVAT(0);
          console.log(this.productToEdit);


        }

    });
  }

  //this function will add iteration on ngFor
  anotherProduct(){
    this.numberOfNewProducts++
  }

//this function adds vat from vat arg i is for index of ngFor
  addVAT(i){
    //get price without vat
    let pwov :number =this.pricesWithoutVAT[i];
    // calc the amount of vat
    let vat:number = (17/100)+1;

    //add vat to price
    let pwv:number = pwov * vat;
    //set price with vat
    this.pricesWithVAT[i] =pwv;

  }

  //this function removes vat from vat arg i is for index of ngFor
  removeVAT(i){
    //get price with vat
    let pwv:number = this.pricesWithVAT[i];
    // calc the amount of vat
    let vat :number= (17/100)+1;
    //subtract vat from price
    let pwov:number = pwv / vat;
    //set the price without vat
    this.pricesWithoutVAT[i] = pwov;

  }

}
