import { Component, OnInit } from '@angular/core';
import {ProductsRestService} from '../products-rest-service/products-rest-service.service';
import {ProductsInterface} from '../products-rest-service/products-interface';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
products;
  constructor(private rest :ProductsRestService) { }

  ngOnInit() {
    this.products = this.rest.products;

  }


  calcVAT(number,VATPrecent){
    return number+(number/100*VATPrecent);
  }
}
