// this component displays thw list of clients
import { Component, OnInit } from '@angular/core';
import { ClientInterface } from '../client-rest-service/client-interface';
import { ClientRestService } from '../client-rest-service/client-rest-service.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css'],

})
export class ClientsComponent implements OnInit {
  clients:ClientInterface[];
  constructor(private rest:ClientRestService) { }

  ngOnInit() {
    this.clients = this.rest.clients;
    console.log(this.clients);

  }

}
