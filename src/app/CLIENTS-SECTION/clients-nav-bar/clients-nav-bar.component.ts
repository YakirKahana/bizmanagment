import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-clients-nav-bar',
  templateUrl: './clients-nav-bar.component.html',
  styleUrls: ['./clients-nav-bar.component.css']
})
export class ClientsNavBarComponent implements OnInit {
  routeID;
  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
      this.routeID = this.route.snapshot.params['clientName'];
      console.log(this.routeID);
  }

}
