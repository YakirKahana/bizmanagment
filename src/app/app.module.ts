/*~~~~~~IMPORTS~~~~~~*/
/* MODULES */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';

/* COMPONENTS */

//general
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import { IncomingOrderComponent } from './incoming-order/incoming-order.component';
import { OutgingOrderComponent } from './outging-order/outging-order.component';


//suppliers
import { SuppliersComponent } from './SUPPLIERS-SECTION/suppliers/suppliers.component';
import { SupplierPageComponent } from './SUPPLIERS-SECTION/supplier-page/supplier-page.component';
import { SupplierInfoNavbarComponent } from './SUPPLIERS-SECTION/supplier-info-navbar/supplier-info-navbar.component';
import { SupplierProductListComponent } from './SUPPLIERS-SECTION/supplier-product-list/supplier-product-list.component';
import { SupplierOrdersComponent } from './SUPPLIERS-SECTION/supplier-orders/supplier-orders.component';
import { NewEditSupplierComponent } from './SUPPLIERS-SECTION/new-edit-supplier/new-edit-supplier.component';
import { SupplierNewEditProductComponent } from './SUPPLIERS-SECTION/new-edit-product/new-edit-product.component';

//clients
import { ClientsNavBarComponent } from './CLIENTS-SECTION/clients-nav-bar/clients-nav-bar.component';
import { ClientInfoPageComponent } from './CLIENTS-SECTION/client-info-page/client-info-page.component';
import { ClientsComponent } from './CLIENTS-SECTION/clients/clients.component';
import { ClientsOrdersComponent } from './CLIENTS-SECTION/clients-orders/clients-orders.component';
import { NewEditClientComponent } from './CLIENTS-SECTION/new-edit-client/new-edit-client.component';

//Products
import { ProductsComponent } from './PRODUCTS-SECTION/products/products.component';
import { NewEditProductComponent } from './PRODUCTS-SECTION/new-edit-product/new-edit-product.component';



/* SERVICES */
import { SupplierRestService } from './SUPPLIERS-SECTION/supplier-rest-service/supplier-rest-service.service';
import {ClientRestService} from './CLIENTS-SECTION/client-rest-service/client-rest-service.service';
import {ProductsRestService} from './PRODUCTS-SECTION/products-rest-service/products-rest-service.service';


/* PIPES */
// loop pipe- makes ngFor work with a number. the number is the number of iterations
import { LoopNumberPipe } from './SUPPLIERS-SECTION/new-edit-product/new-edit-product.component';

// filter pipe- filters ngFor with input for makeing a search
import { FilterPipe } from './filter.pipe';









/* END OF IMPORTS*/

// appRoutes const contains all the routes of the app
const appRoutes:Routes =[
  { path: "home", component: HomeComponent},  //Go to home page
  { path: "", redirectTo: "home",pathMatch: 'full'}, // Redirect To home page
  { path: "suppliers", component: SuppliersComponent}, // Go to suppliers page - display all suppliers
  { path: "clients", component:ClientsComponent}, // Go to clients list - display all clients

    { path: "clients/client-info/:clientName", component:ClientsNavBarComponent, children:[
      {path:"", component:ClientInfoPageComponent},
      {path:"orders", component:ClientsOrdersComponent}
    ]},
    { path: "clients/edit-client/:id", component: NewEditClientComponent}, // edit or add suplier; if id is "new" = new supplier

  // go to Supplier navBar Component - inside router
  {path: 'suppliers/supplierPage/:companyName', component:SupplierInfoNavbarComponent, children:[
      {path: "", component:SupplierPageComponent }, // go to Supplier page - show SPECIFIC supplier info
      {path: "product-list", component:SupplierProductListComponent, }, // go to supplier product list - show SPECIFIC supplier products
      {path: "orders", component:SupplierOrdersComponent, },
  ]},
  { path: "suppliers/edit-product/:id/:productName", component: SupplierNewEditProductComponent},// go to add/edit  page. if productName = "new" will be new products
  { path: "suppliers/edit-supplier/:id", component: NewEditSupplierComponent}, // edit or add suplier; if id is "new" = new supplier
  { path:"products", component:ProductsComponent},
  { path:"products/edit-product/:id", component:NewEditProductComponent},
  {path:"incoming-order", component:IncomingOrderComponent},
  { path:"outgoing-order", component:OutgingOrderComponent},
  { path:"**", component:PageNotFoundComponent},//if url do not exsit - go to 404 page not found



];


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SuppliersComponent,
    ClientsComponent,
    SupplierPageComponent,
    SupplierInfoNavbarComponent,
    SupplierProductListComponent,
    HomeComponent,
    PageNotFoundComponent,
    FilterPipe,
    SupplierNewEditProductComponent,
    LoopNumberPipe,
    NewEditSupplierComponent,
    ProductsComponent,
    SupplierOrdersComponent,
    ClientsNavBarComponent,
    ClientInfoPageComponent,
    ClientsOrdersComponent,
    NewEditClientComponent,
    NewEditProductComponent,
    IncomingOrderComponent,
    OutgingOrderComponent



  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [SupplierRestService,ClientRestService,ProductsRestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
