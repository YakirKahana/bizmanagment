import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierInfoNavbarComponent } from './supplier-info-navbar.component';

describe('SupplierInfoNavbarComponent', () => {
  let component: SupplierInfoNavbarComponent;
  let fixture: ComponentFixture<SupplierInfoNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierInfoNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierInfoNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
