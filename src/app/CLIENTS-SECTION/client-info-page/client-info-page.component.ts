import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {ClientInterface } from '../client-rest-service/client-interface';
import {ClientRestService} from '../client-rest-service/client-rest-service.service';

@Component({
  selector: 'app-client-info-page',
  templateUrl: './client-info-page.component.html',
  styleUrls: ['./client-info-page.component.css'],

})
export class ClientInfoPageComponent implements OnInit {

  clientName;
  clientData;

  constructor(private rest:ClientRestService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.clientName = this.route.snapshot.params['clientName'];
    this.clientData = this.rest.clients.filter(obj => obj.clientName == this.clientName);
    console.log(this.clientData);
  }

}
