import { Injectable } from '@angular/core';
import { ProductsInterface } from './products-interface';


@Injectable()
export class ProductsRestService {
  products :ProductsInterface[] =[{
    productName:"product1",
    catalogNumber:1,
    priceWithoutVAT:1,
  },{
    productName:"product2",
    catalogNumber:2,
    priceWithoutVAT:20,
  },{
    productName:"product3",
    catalogNumber:3,
    priceWithoutVAT:300,
  },{
    productName:"product4",
    catalogNumber:4,
    priceWithoutVAT:4000,
  },{
    productName:"product5",
    catalogNumber:5,
    priceWithoutVAT:5,
  },{
    productName:"product6",
    catalogNumber:6,
    priceWithoutVAT:6,
  },{
    productName:"product7",
    catalogNumber:7,
    priceWithoutVAT:7,
  },{
    productName:"product8",
    catalogNumber:8,
    priceWithoutVAT:8,
  },{
    productName:"product9",
    catalogNumber:9,
    priceWithoutVAT:9,
  },{
    productName:"product10",
    catalogNumber:10,
    priceWithoutVAT:10,
  },{
    productName:"product11",
    catalogNumber:11,
    priceWithoutVAT:11,
  },{
    productName:"product12",
    catalogNumber:12,
    priceWithoutVAT:12,
  },{
    productName:"product13",
    catalogNumber:13,
    priceWithoutVAT:13,
  },{
    productName:"product14",
    catalogNumber:14,
    priceWithoutVAT:14,
  },{
    productName:"product15",
    catalogNumber:15,
    priceWithoutVAT:15,
  },];
  constructor() { }

}
