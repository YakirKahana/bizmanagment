import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {SuppliersInterface} from '../supplier-rest-service/suppliers-interface';
import {SupplierRestService} from '../supplier-rest-service/supplier-rest-service.service';

@Component({
  selector: 'app-new-edit-supplier',
  templateUrl: './new-edit-supplier.component.html',
  styleUrls: ['./new-edit-supplier.component.css'],

})
export class NewEditSupplierComponent implements OnInit {

 supplier:SuppliersInterface[]; // supplier info
 companyName; // route parameter

  constructor(private route:ActivatedRoute, private rest:SupplierRestService) { }

  ngOnInit() {
    // get route parameter and assign it to companyName var
    this.companyName = this.route.snapshot.params['id'] ;

    // get supllier data from service based on the companyName var and assign the data to supplier variable
    this.supplier = this.rest.suppliersList.filter(obj => obj.companyName ==   this.companyName);

  }

}
