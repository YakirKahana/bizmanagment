import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditSupplierComponent } from './new-edit-supplier.component';

describe('NewEditSupplierComponent', () => {
  let component: NewEditSupplierComponent;
  let fixture: ComponentFixture<NewEditSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
