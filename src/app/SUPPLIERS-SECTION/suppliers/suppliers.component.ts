/* this component displays a list of all the suppliers to the user*/
import { Component, OnInit } from '@angular/core';
 // import SupplierRestService to get supplier data.
import { SupplierRestService } from '../supplier-rest-service/supplier-rest-service.service';
import {SuppliersInterface} from '../supplier-rest-service/suppliers-interface';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css'],
  providers:[SupplierRestService]
})
export class SuppliersComponent {
  term;
  suppliers; //list of suppliers
    constructor(private rest:SupplierRestService){
      // get the list of suppliers from the rest service and assign it to the "suppliers" variable
        this.suppliers = rest.suppliersList;
    }

}
