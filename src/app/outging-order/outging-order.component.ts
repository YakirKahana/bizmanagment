import { Component, OnInit } from '@angular/core';
import { ClientRestService } from '../CLIENTS-SECTION/client-rest-service/client-rest-service.service';
import { ProductsRestService } from '../PRODUCTS-SECTION/products-rest-service/products-rest-service.service';

@Component({
  selector: 'app-outging-order',
  templateUrl: './outging-order.component.html',
  styleUrls: ['./outging-order.component.css']
})
export class OutgingOrderComponent implements OnInit {
  numberOfProducts = 1;
  clients;
  selectedClient;
  products;
  totalPrices = [];


  order = {
    client: "",
    products: []
  };

  constructor(private clientRest: ClientRestService, private productRest: ProductsRestService) { }

  ngOnInit() {
    this.clients = this.clientRest.clients;

  }

  getProducts() {
    this.order.client = this.selectedClient;
    this.products = this.productRest.products;
    console.log(this.products);

  }

  completeForm(whatParam, index, param) {
    switch (whatParam) {
      case 'catNum':
        let temp = this.products.filter(obj2 => obj2.catalogNumber == param);
        this.order.products[index] = temp[0];

        console.log(this.products);

        break;

      case 'prodName':
        let temp2 = this.products.filter(obj2 => obj2.productName == param);
        this.order.products[index] = temp2[0];
        console.log(param);
        console.log(this.products);
        console.log(this.order);
        break;
    }
  }

  addProduct() {
    this.numberOfProducts++;
  }

  changeTotalPrice(price, amout, index) {
    this.totalPrices[index] = price * amout;
  }

  addPrices(total, num) {
    return total + num;
  }
}
