import { BizManagmentPage } from './app.po';

describe('biz-managment App', () => {
  let page: BizManagmentPage;

  beforeEach(() => {
    page = new BizManagmentPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
