import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutgingOrderComponent } from './outging-order.component';

describe('OutgingOrderComponent', () => {
  let component: OutgingOrderComponent;
  let fixture: ComponentFixture<OutgingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutgingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutgingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
