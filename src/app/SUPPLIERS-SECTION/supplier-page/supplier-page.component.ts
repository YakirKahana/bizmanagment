/* this component displays a SPECIFIC supplier info - gets the data from SupplierRestService  show the data based of route parameter */

import { Component, OnInit} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {SuppliersInterface} from '../supplier-rest-service/suppliers-interface';
import {SupplierRestService} from '../supplier-rest-service/supplier-rest-service.service';



@Component({
  selector: 'app-supplier-page',
  templateUrl: './supplier-page.component.html',
  styleUrls: ['./supplier-page.component.css'],

})
export class SupplierPageComponent implements OnInit {

  companyName; // route parameter

  supplier:SuppliersInterface[] =[]; // supplier info

  constructor(private rest:SupplierRestService,private route : ActivatedRoute) { }

  ngOnInit() {
    // get route parameter and assign it to companyName var
    this.companyName = this.route.snapshot.params['companyName'] ;

    // get supllier data from service based on the companyName var and assign the data to supplier variable
    this.supplier = this.rest.suppliersList.filter(obj => obj.companyName ==   this.companyName);

  }

}
