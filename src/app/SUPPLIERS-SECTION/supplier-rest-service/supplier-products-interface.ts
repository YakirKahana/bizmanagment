export interface SupplierProductsInterface {
  companyName:string,
  products:[{
    productName:string,
    catalogNumber:number,
    price:number,
  }]

}
