import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {ClientInterface} from '../client-rest-service/client-interface';
import {ClientRestService} from '../client-rest-service/client-rest-service.service';

@Component({
  selector: 'app-new-edit-client',
  templateUrl: './new-edit-client.component.html',
  styleUrls: ['./new-edit-client.component.css'],
  
})
export class NewEditClientComponent implements OnInit {
client:ClientInterface[];
clientName;
  constructor(private route:ActivatedRoute,private rest:ClientRestService) { }

  ngOnInit() {
    this.clientName = this.route.snapshot.params['id'];
    if(this.clientName != 'new'){
      this.client = this.rest.clients.filter(obj => obj.clientName == this.clientName);
    }


  }

}
