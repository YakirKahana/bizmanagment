import { TestBed, inject } from '@angular/core/testing';

import { ProductsRestService } from './products-rest-service.service';

describe('ProductsRestServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsRestService]
    });
  });

  it('should be created', inject([ProductsRestService], (service: ProductsRestService) => {
    expect(service).toBeTruthy();
  }));
});
