import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierNewEditProductComponent } from './new-edit-product.component';

describe('NewEditProductComponent', () => {
  let component: SupplierNewEditProductComponent;
  let fixture: ComponentFixture<SupplierNewEditProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierNewEditProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierNewEditProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
