/* this pipe filters ngFor with an input to preform a search */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  //items argument = the items to filter. term argument = the term to filter by - user input
  transform(items: any, term: any): any {
      //cheack if term is undefined. if so- return everything / don't preform filtering
      if(term === undefined){
        return items;
      }
      // update items array by the term. then, return the updated array.
      return items.filter((item)=>{
        if(item.productName){
          return item.productName.toLowerCase().includes(term.toLowerCase());
        }else if(item.companyName){
          return item.companyName.toLowerCase().includes(term.toLowerCase());
        }else if (item.clientName){
          return item.clientName.toLowerCase().includes(term.toLowerCase());
        }else{
          return;
        }

      })
  }

}
