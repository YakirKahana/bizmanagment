import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {SuppliersInterface} from '../supplier-rest-service/suppliers-interface';
import {SupplierRestService} from '../supplier-rest-service/supplier-rest-service.service';
@Component({
  selector: 'app-supplier-info-navbar',
  templateUrl: './supplier-info-navbar.component.html',
  styleUrls: ['./supplier-info-navbar.component.css']
})
export class SupplierInfoNavbarComponent implements OnInit {

  key; // key is the route parameter
  constructor(private route : ActivatedRoute) {
   }

  ngOnInit() {
      // get route parameters and assign it to "key" variable
      this.key = this.route.snapshot.params['companyName'];
      this.route.params.subscribe((params) => {
        this.key = params['companyName'];

      });
  }



}
