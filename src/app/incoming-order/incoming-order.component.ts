import { Component, OnInit, ElementRef } from '@angular/core';
import {SupplierRestService} from '../SUPPLIERS-SECTION/supplier-rest-service/supplier-rest-service.service';

@Component({
  selector: 'app-incoming-order',
  templateUrl: './incoming-order.component.html',
  styleUrls: ['./incoming-order.component.css']
})
export class IncomingOrderComponent implements OnInit {
  numberOfProducts = 1;
  suppliers;
  selectedSupplier;
  selectedSupplierProducts;
  totalPrices=[];


  order= {
    supplier:"",
    products:[]
  };

  constructor(private supplierRest:SupplierRestService) { }

  ngOnInit() {
    this.suppliers = this.supplierRest.suppliersList;

  }

  getProducts(){
    this.order.supplier = this.selectedSupplier;
    let ProductsObj;
    ProductsObj = this.supplierRest.productsList.filter(obj => obj.companyName == this.selectedSupplier);
    this.selectedSupplierProducts = ProductsObj[0].products;

  }

  completeForm(whatParam,index,param){
    switch (whatParam){
      case 'catNum':
        let temp = this.selectedSupplierProducts.filter(obj2 => obj2.catalogNumber == param);
        this.order.products[index] = temp[0];
        console.log(param);
        console.log(this.selectedSupplierProducts);
        console.log(this.order);
      break;

      case 'prodName':
      let temp2 = this.selectedSupplierProducts.filter(obj2 => obj2.productName == param);
      this.order.products[index] = temp2[0];
      console.log(param);
      console.log(this.selectedSupplierProducts);
      console.log(this.order);
      break;
    }
  }

  addProduct(){
    this.numberOfProducts++;
  }

  changeTotalPrice(price,amout,index){
      this.totalPrices[index] = price * amout;
  }

  addPrices(total, num) {
    return total + num;
}

}
